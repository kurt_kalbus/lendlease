package lendlease.database;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import lendlease.TransHdrRecord;

/**
 *
 * @author kkalb
 */
public class DBTransHdr {

    private static PreparedStatement findTransactionsByDate;
    private static PreparedStatement findTransactionsByDateOrderByTime;
    private static PreparedStatement findTransactionsByDateAndAssociate;
    private static PreparedStatement findTransactionsByMemberIdPerMonth;
    private static PreparedStatement findSaleHdrIdByMonth;
    private static PreparedStatement findInvenReceiptsByMonth;
    private static PreparedStatement findTransactionDateBySaleHdrId;
    private static PreparedStatement updateTransCnt;
    private static PreparedStatement findTransactionsByMonth;
    private static PreparedStatement findTransactionsByMonthAndAssociate;
    private static ResultSet rs;
    
    private static String sql10 = "update trans_hdr set trans_cnt =  ? "
                + "where trans_hdr_id = ?";
        
    public static void init() {
      
         String sql1 = "select trans_hdr_id, customer_id, associate_id, trans_amt, trans_dt, trans_cnt, "
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi')";
        
         String sql2 = "select trans_hdr_id, trans_amt, trans_cnt,"
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi') and associate_id = ? "
                + " order by time";
         
         System.out.println ("sql2 = "+sql2);
         
         String sql3 = "select trans_hdr_id, trans_amt, trans_cnt,"
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi') order by trans_dt";
       
        String sql4 = "select trans_hdr_id, trans_amt, trans_cnt, " 
                +"to_char(trans_dt, 'DD-Mon-YYYY') as saledate from trans_hdr "
                +"where to_char (trans_dt, 'Mon-YYYY') = ? and customer_id = ? order by saledate";
        
        String sql5 = "select trans_hdr_id from trans_hdr where to_char (trans_dt, 'Mon-YYYY') = ? ";
        
        String sql6 = "select trans_dt from trans_hdr where trans_hdr_id = ? ";
        
        String sql7 = "select trans_hdr_id, reference_txt, trans_dt from trans_hdr "
                + "where to_char (trans_dt, 'Mon-YYYY') = ? and trans_type_cd = 'I' "
                + "order by reference_txt";
        
        String sql8 = "select trans_hdr_id, trans_amt, trans_cnt, trans_type_cd, associate_id, " 
                + "to_char(trans_dt, 'DD-Mon-YYYY') as saledate from trans_hdr "
                + "where to_char (trans_dt, 'Mon-YYYY') = ? order by saledate";
        
        String sql9 = "select trans_hdr_id, trans_amt, trans_cnt, trans_type_cd, associate_id, " 
                + "to_char(trans_dt, 'DD-Mon-YYYY') as saledate from trans_hdr "
                + "where to_char (trans_dt, 'Mon-YYYY') = ? and associate_id = ? "
                + "order by saledate";
        
        try {
            findTransactionsByDate = DBUtils.getConnection().prepareStatement (sql1);
            findTransactionsByDateAndAssociate = DBUtils.getConnection().prepareStatement (sql2);
            findTransactionsByDateOrderByTime = DBUtils.getConnection().prepareStatement (sql3);
            findTransactionsByMemberIdPerMonth = DBUtils.getConnection().prepareStatement (sql4);
            findSaleHdrIdByMonth = DBUtils.getConnection().prepareStatement (sql5);
            findTransactionDateBySaleHdrId = DBUtils.getConnection().prepareStatement (sql6);
            findInvenReceiptsByMonth = DBUtils.getConnection().prepareStatement (sql7);
            findTransactionsByMonth = DBUtils.getConnection().prepareStatement (sql8);
            findTransactionsByMonthAndAssociate = DBUtils.getConnection().prepareStatement (sql9);
               
        } catch (java.sql.SQLException ex) {
            System.out.println("Preparing select statement: " + ex.getMessage());
        }
    }
    
    public static String getCustIDFromTransHdrId (String trans_hdr_id) {
        
        String query = "select customer_id from trans_hdr where trans_hdr_id = '"
                     + trans_hdr_id+ "'";
        
        String custId = null;
        
         try {
            Statement ss = DBUtils.getConnection().createStatement();
           
            ResultSet result = ss.executeQuery(query);
            
            if (result.next()) {
                custId = result.getString("customer_id");
                System.out.println("customer_id = " + custId);
            }
            result.close();
          
         } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }

         return custId;
    }

    public static String getLastSaleReceiptNumber() {
        
        String query = "select max(trans_hdr_id) from trans_hdr where trans_type_cd='S'";
        String receiptNumber = "";;
        
         try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            ResultSet result = ss.executeQuery(query);
            if(result.next()) {

                receiptNumber = result.getString(1);
            }
            result.close();
           
         } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
         
         return receiptNumber;
    }
    
    public static void updateTransCnt (int trans_cnt, String trans_hdr_id) {
        
        try {
            updateTransCnt = DBUtils.getConnection().prepareStatement (sql10);
            
            updateTransCnt.setInt(1, trans_cnt);
            updateTransCnt.setString(2, trans_hdr_id);
            
            int num = updateTransCnt.executeUpdate();
            
            System.out.println (" updated " + num + " trans_cnt");
            updateTransCnt.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred updating trans_hdr: " + ex.getMessage());
        }
    }
    
    /*
     * 
     */
    public static ArrayList<String> findSaleHdrIdsByMonth (String month) {
       
        ArrayList<String> saleHdrIds = new ArrayList<String>();
        try {

            findSaleHdrIdByMonth.setString(1,month);
            rs = findSaleHdrIdByMonth.executeQuery();
         
            while (rs.next()) {

                saleHdrIds.add(rs.getString("trans_hdr_id"));
            }
            
            rs.close();
          
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        
        return saleHdrIds;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord> findTransactionsByDate (String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {

            findTransactionsByDate.setString(1,date+":00:00");
            findTransactionsByDate.setString(2,date+":23:59");
            rs = findTransactionsByDate.executeQuery();
         
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String customer_id = rs.getString("customer_id");
                String associate_id = rs.getString("associate_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_dt = rs.getString("trans_dt");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_time = rs.getString("time").substring(12);
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_dt, trans_cnt, customer_id,
                                           associate_id, trans_amt, trans_time, "", null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    public static ArrayList<TransHdrRecord> findTransactionsByDateOrderByTime (String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {

            findTransactionsByDateOrderByTime.setString(1,date+":00:00");
            findTransactionsByDateOrderByTime.setString(2,date+":23:59");
            rs = findTransactionsByDateOrderByTime.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_time = rs.getString("time").substring(12);
                int trans_cnt = rs.getInt("trans_cnt");
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, date, trans_cnt, "",
                                            "", trans_amt, trans_time, "", null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        
        return transactions;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord> findTransactionsByMonth (String date) {
            
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
                
        try {

            findTransactionsByMonth.setString(1,date);
            rs = findTransactionsByMonth.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_date = rs.getString("saledate");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_type_cd = rs.getString ("trans_type_cd");
                String assoc_id = rs.getString("associate_id");
               
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_date, trans_cnt, 
                                            "", assoc_id, trans_amt, null,
                                            trans_type_cd,  null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        
        return transactions;
    }
    
    /*
     * 
     */
    public static TransHdrRecord findTransactionByHdrId (String hdrId) {
            
        TransHdrRecord record = null ;
        
        String sql = "select trans_hdr_id, customer_id, associate_id, "
                + "trans_type_cd, trans_amt,reference_txt, trans_cnt, "
                + "trans_dt, to_char(trans_dt, 'DD-MON-YYYY hh24:mi am') "
                + "as time from trans_hdr where trans_hdr_id = "+hdrId;
            
        try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            ResultSet result = ss.executeQuery(sql);
            if(result.next()) {

                String trans_hdr_id = result.getString("trans_hdr_id");
                String customer_id = result.getString("customer_id");
                String associate_id = result.getString("associate_id");
                String trans_type_cd = result.getString("trans_type_cd");
                String trans_amt = result.getString("trans_amt");
                String reference_txt = result.getString("reference_txt");
                int count = result.getInt("trans_cnt");
                String date = result.getString("trans_dt");
                String time = result.getString("time");
            
                record = new TransHdrRecord (trans_hdr_id, date, count,
                            customer_id, associate_id, trans_amt, time,
                            trans_type_cd, reference_txt);
            }
                        
            result.close();
           
         } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());         
        }
        
        return record;
    }
    
     /*
     * 
     */
    public static ArrayList<TransHdrRecord>
            findTransactionsByMonthAndAssociate (String date, String assocId) {
            
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
                
        try {

            findTransactionsByMonthAndAssociate.setString(1,date);
            findTransactionsByMonthAndAssociate.setString(2,assocId);
            rs = findTransactionsByMonthAndAssociate.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_date = rs.getString("saledate");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_type_cd = rs.getString ("trans_type_cd");
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_date, trans_cnt, 
                                            "", assocId, trans_amt, null,
                                            trans_type_cd,  null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord>
            findTransactionsByMemberIdPerMonth (String memberId, String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
                
        try {

            findTransactionsByMemberIdPerMonth.setString(1,date);
            findTransactionsByMemberIdPerMonth.setString(2,memberId);
            rs = findTransactionsByMemberIdPerMonth.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_date = rs.getString("saledate");
                int trans_cnt = rs.getInt("trans_cnt");
               
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_date, trans_cnt, 
                                            memberId, null, trans_amt, null, "", null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static String findTransactionDate (String saleHdrId) {
            
        String trans_dt = null;
                
        try {

            findTransactionDateBySaleHdrId.setString(1,saleHdrId);
            rs = findTransactionDateBySaleHdrId.executeQuery();
           
            if (rs.next()) {

                trans_dt = rs.getString("trans_dt");
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        
        return trans_dt;
    }
}
