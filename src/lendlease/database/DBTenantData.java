package lendlease.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import lendlease.TenantData;

/**
 *
 * @author kkalb
 */
public class DBTenantData {

    private static PreparedStatement insertData;
    private static ResultSet rs;
    
     public static TenantData getTenantData (String interface_nm) {

        TenantData tenantData = null;
        
         try {
             
             String sql =  "select mall_id, store_id, pos_id, batch_id, ftp_url, "
                + "to_char(update_dt, 'DD-MON-YYYY hh24:mi') as time from tenant_data "
                + "where interface_nm='" +interface_nm+ "'";
             
             PreparedStatement getTenantData =
                     DBUtils.getConnection().prepareStatement (sql);
               //   "select mall_id, store_id, pos_id, batch_id, ftp_url, "
               // + "to_char(update_dt, 'DD-MON-YYYY hh24:mi') as time from tenant_data "
               // + "where interface_nm='" +interface_nm+ "'");
             
            rs = getTenantData.executeQuery();
            
            while (rs.next()) {
                String mall_id = rs.getString("mall_id");
                String store_id = rs.getString("store_id");
                String pos_id = rs.getString("pos_id");
                int batch_id = rs.getInt("batch_id");
                String ftp_url = rs.getString("ftp_url");
                String update_dt = rs.getString("time");
                
                tenantData = new TenantData (interface_nm, mall_id, store_id,
                                            pos_id, batch_id, ftp_url, update_dt);
            }
            
        } catch (java.sql.SQLException ex) {
            
            System.out.println("Failed to find tenant information: " + ex.getMessage());
        }
         
        return tenantData;
    }
  
    public static ArrayList<TenantData> getTenantData () {

        ArrayList<TenantData> tenantData = new ArrayList<TenantData>();
        
         try {
             
             PreparedStatement getTenantData = DBUtils.getConnection().prepareStatement (
                  "select interface_nm, mall_id, store_id, pos_id, batch_id, ftp_url, "
                + "to_char(update_dt, 'DD-MON-YYYY hh24:mi') as time from tenant_data");
             
            rs = getTenantData.executeQuery();
            
            while (rs.next()) {
                String interface_nm = rs.getString("interface_nm");
                String mall_id = rs.getString("mall_id");
                String store_id = rs.getString("store_id");
                String pos_id = rs.getString("pos_id");
                int batch_id = rs.getInt("batch_id");
                String ftp_url = rs.getString("ftp_url");
                String update_dt = rs.getString("time");
                
                tenantData.add(
                        new TenantData (interface_nm, mall_id, store_id, pos_id,
                                        batch_id, ftp_url, update_dt));
            }
            
        } catch (java.sql.SQLException ex) {
            
            System.out.println("Failed to find tenant information: " + ex.getMessage());
        }
         
        return tenantData;
    }
    
    public static boolean updateData (String interface_nm, String mall_id,
                                   String store_id, String pos_id,
                                   int batch_id, String ftp_url) {
        
        String timeStamp = "";
        boolean success = true;
     
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Calendar today = Calendar.getInstance();
        timeStamp = dateFormat.format(today.getTime());
        
        String updateSQL = "update tenant_data set "
                + "mall_id = '" +mall_id+ "', "
                + "store_id ='"  +store_id+ "', "
                + "pos_id = '"+pos_id+"', "
                + "batch_id = "+ String.valueOf(batch_id) +", "
                + "ftp_url = " +ftp_url+ ", "
                + "update_dt = to_date('" +timeStamp+ "', 'DD-MON-YYYY HH24:MI') "
                + "where interface_nm = '"+interface_nm+"'";
        
        System.out.println (" updateSQL + "+updateSQL);
        
        try {   
            PreparedStatement updateData =
                    DBUtils.getConnection().prepareStatement (updateSQL); 
        
            int cnt = updateData.executeUpdate();
            updateData.close();
            System.out.println (" update tenant update count = "+cnt);
            
            if (cnt == 0) {
                
                // Update failed - there is no record for this item for the given
                // month, so create one.
                
                String insertSQL = "insert into tenant_data "
                                + "(interface_nm, mall_id, store_id, pos_id, "
                                + "batch_id, ftp_url, update_dt) "
                                + "values(?, ?, ?, ?, ?, ?, ?)";
                
                insertData = DBUtils.getConnection().prepareStatement (insertSQL); 
                
                insertData.setString(1,interface_nm);
                insertData.setString(2,mall_id);
                insertData.setString(3,store_id);
                insertData.setString(4,pos_id);
                insertData.setInt(5,batch_id);
                insertData.setString(6,ftp_url);
                
                insertData.setString(7, "to_date('" +timeStamp+ "', 'DD-MON-YYYY HH24:MI')");
                
                cnt = insertData.executeUpdate();
                insertData.close();
                System.out.println (" item_activity insert count = "+cnt);
            }
                
        }  catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
            success = false;
        }
        return success;
    }
    
    public static boolean deleteInterface (String interface_nm) {
        
        boolean success = true;
        
        String deleteSQL = "delete from tenant_data where interface_nm='"
                + interface_nm +"'";
        
        try {   
            PreparedStatement deleteData =
                    DBUtils.getConnection().prepareStatement (deleteSQL); 
        
            int cnt = deleteData.executeUpdate();
            deleteData.close();
            System.out.println (" update tenant update count = "+cnt);
            
        }  catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
            success = false;
        }
        
        return success;
    }
    
    public static boolean insertData (String interface_nm, String mall_id,
                                      String store_id, String pos_id,
                                      int batch_id, String ftp_url) {
        
        boolean success = true;
        String timeStamp = "";
     
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Calendar today = Calendar.getInstance();
        timeStamp = dateFormat.format(today.getTime());
        
        String insertSQL = "insert into tenant_data "
                + "(interface_nm, mall_id, store_id, pos_id, batch_id, ftp_url) "
                + "values('"  +interface_nm+ "','"+mall_id+ "','" 
                +  store_id+ "','" +pos_id+ "','" +batch_id+ "','" +ftp_url+ "')";
        
        System.out.println (" insertSQL = "+ insertSQL);
          
        try {   
          
            insertData = DBUtils.getConnection().prepareStatement (insertSQL); 
            
            int cnt = insertData.executeUpdate();
            insertData.close();
            System.out.println (" tenant data insert count = "+cnt);
              
        }  catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
            success = false;
        }
        
        return success;
    }
}
