/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lendlease;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author kkalb
 */
public class LendLeaseRecord {
    
    private int machine_id = 0;
    private String date = "";
    private int batch_id = 0;
    private String hour = "";
    private int num_trans = 0;
    private BigDecimal total_sales = new BigDecimal ("0.00", LendLease.mc);
    private BigDecimal gst = new BigDecimal ("0.00", LendLease.mc);
    private BigDecimal discount = new BigDecimal ("0.00", LendLease.mc);
    private int num_pax = 0;

    public LendLeaseRecord (int machine_id, int batch_id, String hour) {
                  
        this.machine_id = machine_id;
        this.batch_id = batch_id;
        this.hour = hour;
    }
    
    public void addItem (BigDecimal amount) {
        
        total_sales = total_sales.add(amount);
        num_trans++;
    }
    
    public String get_hour() {
        return hour;
    }
    
    public BigDecimal get_total_sales() {
        
        return total_sales;
    }
    
    public int get_num_trans() {
        
        return num_trans;
    }
    
    
}
